//
//  NewMessageTableViewController.swift
//  Chat
//
//  Created by Tamir Shklaz on 2018/01/10.
//  Copyright © 2018 Tamir Shklaz. All rights reserved.
//

import UIKit
import Firebase
import SDWebImage
import FirebaseStorageUI


class NewMessageTableViewController: UITableViewController {
    
    let cellId = "cellId"
    
    var users = [User]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(handleCancel))
        
        tableView.register(UserCell.self, forCellReuseIdentifier: cellId)
        
        fetchUsers()


    }
    //MARK: Firebase
    
    func fetchUsers(){
        Database.database().reference().child("users").observe(.childAdded, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String: Any]{
                let name = dictionary["name"] as? String
                let email = dictionary["email"] as? String
                let uid = dictionary["uid"] as? String
                
                let user = User(name: name!, email: email!, uid: uid!)
                
                self.users.append(user)
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
            
        }, withCancel: nil)
        
    }
    
    //MARK: Actions
    @objc func handleCancel(){
        dismiss(animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! UserCell
        
        let user = users[indexPath.row]
        cell.textLabel?.text = user.name
        cell.detailTextLabel?.text = user.email
        
        if let uid = user.uid{
            let path =  "profile_images/"+uid
            let ref = Storage.storage().reference(withPath: path)
            cell.profileImageView.sd_setImage(with: ref)
           
            
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    var messegesController: MessagesController?
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: true, completion: nil)
        print("Dissmising row: \(indexPath) ")
        let user = users[indexPath.row]
        self.messegesController?.showChatController(user: user)
    }

}


