//
//  LoginController+handlers.swift
//  Chat
//
//  Created by Tamir Shklaz on 2018/01/10.
//  Copyright © 2018 Tamir Shklaz. All rights reserved.
//

import Foundation
import UIKit

extension LoginController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    @objc func handleSelectProfileImageView(){
        print("Image Clicked")
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        
        present(picker, animated: true, completion: nil)
        
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("Picker Canceled")
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var selectedImageFromPicker: UIImage?
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            selectedImageFromPicker = editedImage
            
        }else if let orignialImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            selectedImageFromPicker = orignialImage
            
        }
        
        if let selectedImage = selectedImageFromPicker{
            
            profileImageView.layer.cornerRadius = 75
            profileImageView.layer.masksToBounds = true
            profileImageView.image = selectedImage
            
        }
        
        
        dismiss(animated: true, completion: nil)
    }
    
}
