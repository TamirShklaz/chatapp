//
//  ViewController.swift
//  Chat
//
//  Created by Tamir Shklaz on 2018/01/09.
//  Copyright © 2018 Tamir Shklaz. All rights reserved.
//

import UIKit
import Firebase

class MessagesController: UITableViewController {
    
    
    var messages = [Message]()
    var messagesDictionary = [String: Message]()
    let cellId = "cellId"
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkIfUserIsLoggedIn()
      
        let image = UIImage(named: "new_message_icon-1")
        
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(handleLogout))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: image , style: .plain, target: self, action: #selector(handleNewMessage))
        
        tableView.register(UserCell.self, forCellReuseIdentifier: cellId)
        
        observeUserMessages()
        
    }
    
    func observeUserMessages(){
        guard let uid = Auth.auth().currentUser?.uid else{
            return
        }
        
        let userMessagesRef = Database.database().reference().child("user-messages").child(uid)
        userMessagesRef.observe(.childAdded) { (snapshot) in
            let messageId = snapshot.key
            let messagesReference = Database.database().reference().child("messages").child(messageId)
            
            messagesReference.observeSingleEvent(of: .value, with: { (snapshot) in
                
                if let dictionary = snapshot.value as? [String: Any]{
                    
                    let fromId = dictionary["fromId"] as? String
                    let text = dictionary["text"] as? String
                    let timeStamp = dictionary["timeStamp"] as? NSNumber
                    let toId = dictionary["toId"] as? String
                    let message = Message(fromId: fromId!, text: text!, timeStamp: timeStamp!, toId: toId!)
                    
                    
                    
                    if let toId = message.toId{
                        self.messagesDictionary[toId] = message
                        
                        self.messages = Array(self.messagesDictionary.values)
                        self.messages.sort(by: { (message1, message2) -> Bool in
                            return message1.timeStamp!.intValue > message2.timeStamp!.intValue
                        })
                    }
                    
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                    
                }
                
            }, withCancel: nil)
            
            
        }
    
    }
    
    func observeMessages(){
        let ref = Database.database().reference().child("messages")
        ref.observe(.childAdded, with: { (snapshot) in
          
            
            
        }, withCancel: nil)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! UserCell
        let message = messages[indexPath.row]
        cell.message = message
        return cell
    }

    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    
    //MARK: Login Flow
    func checkIfUserIsLoggedIn(){
        if Auth.auth().currentUser?.uid == nil{
            //user is not logged in
            //Only present login screen when it is loaded
            perform(#selector(handleLogout), with: nil, afterDelay: 0)
            
        }else{
            
            setupNavBar()
            
        }
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setupNavBar()
        
        
        }
    
    
    
    func setupNavBar(){
        print("Setting Up Nav Bar")
        if let currentUser = Auth.auth().currentUser{
            
            let titleView = UIButton()
            titleView.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
            self.navigationItem.titleView = titleView
            
            let containerView = UIView()
            containerView.translatesAutoresizingMaskIntoConstraints = false
            titleView.addSubview(containerView)
            
            let profileImageView = UIImageView()
            profileImageView.translatesAutoresizingMaskIntoConstraints = false
            profileImageView.contentMode = .scaleAspectFill
            profileImageView.layer.cornerRadius = 20
            profileImageView.layer.masksToBounds = true
            
            let nameLabel = UILabel()
            nameLabel.translatesAutoresizingMaskIntoConstraints = false
            nameLabel.text = currentUser.displayName
            containerView.addSubview(profileImageView)
            containerView.addSubview(nameLabel)
            
            let path =  "profile_images/"+currentUser.uid
            let profileImageRef = Storage.storage().reference(withPath: path)
            profileImageView.sd_setImage(with: profileImageRef)
            
            profileImageView.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
            profileImageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
            profileImageView.widthAnchor.constraint(equalToConstant: 40).isActive = true
            profileImageView.heightAnchor.constraint(equalToConstant: 40).isActive = true
            
            nameLabel.leftAnchor.constraint(equalTo: profileImageView.rightAnchor, constant: 8).isActive = true
            nameLabel.centerYAnchor.constraint(equalTo: profileImageView.centerYAnchor).isActive = true
            nameLabel.rightAnchor.constraint(equalTo: containerView.rightAnchor).isActive = true
            nameLabel.heightAnchor.constraint(equalTo: profileImageView.heightAnchor).isActive = true
            
            containerView.centerYAnchor.constraint(equalTo: titleView.centerYAnchor).isActive = true
            containerView.centerXAnchor.constraint(equalTo: titleView.centerXAnchor).isActive = true
    
            titleView.addTarget(self, action: #selector(showChatController), for: .touchUpInside)
            
           
            
        }
        
        
        
    }
    @objc func showChatController(user: User){
        print("Showing Chat Controller")
        let chatLogController = ChatLogController(collectionViewLayout: UICollectionViewFlowLayout())
        chatLogController.user = user
        navigationController?.pushViewController(chatLogController, animated: true)
    }
    
    //MARK: Actions
    @objc func handleLogout(){
        
        do{
            try Auth.auth().signOut()
        }catch let logoutError{
            print(logoutError)
        }
        let loginController = LoginController()
        loginController.messagesController = self
        
        present(loginController, animated: true, completion: nil)
        
    }
    
    @objc func handleNewMessage(){
        let newMessageTableViewController = NewMessageTableViewController()
        newMessageTableViewController.messegesController = self
        let navController = UINavigationController(rootViewController: newMessageTableViewController)
        present(navController, animated: true, completion: nil)
    }

    


}

