//
//  User.swift
//  Chat
//
//  Created by Tamir Shklaz on 2018/01/10.
//  Copyright © 2018 Tamir Shklaz. All rights reserved.
//

import UIKit

class User: NSObject {
    
    var name: String?
    var email: String?
    var uid: String?
    
    init(name: String, email: String, uid: String) {
        self.name = name
        self.email = email
        self.uid = uid
    }
    
    

}
