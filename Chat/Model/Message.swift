//
//  Message.swift
//  Chat
//
//  Created by Tamir Shklaz on 2018/01/11.
//  Copyright © 2018 Tamir Shklaz. All rights reserved.
//

import UIKit

class Message: NSObject {
    
    var fromId: String?
    var text: String?
    var timeStamp: NSNumber?
    var toId: String?
    
    init(fromId: String, text: String, timeStamp: NSNumber, toId: String) {
        self.fromId = fromId
        self.timeStamp = timeStamp
        self.text = text
        self.toId = toId
    }

}
