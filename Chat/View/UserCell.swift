//
//  UserCell.swift
//  Chat
//
//  Created by Tamir Shklaz on 2018/01/12.
//  Copyright © 2018 Tamir Shklaz. All rights reserved.
//

import UIKit
import Firebase

class UserCell: UITableViewCell{
    
    var message: Message? {
        didSet{
            if let toId = message?.toId{
                let userRef = Database.database().reference().child("users").child(toId)
                userRef.observe(.value, with: { (snapshot) in
                    if let dictionary = snapshot.value as? [String: Any]{
                        self.textLabel?.text = dictionary["name"] as? String
                        
                        let path =  "profile_images/"+toId
                        let ref = Storage.storage().reference(withPath: path)
                        self.profileImageView.sd_setImage(with: ref)
                        
                        
                        
                    }
                })
            }
            self.detailTextLabel?.text = message?.text
            if let seconds = message?.timeStamp?.doubleValue{
                let timeStampDate = NSDate(timeIntervalSince1970: seconds)
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "hh:mm:ss a"
                timeLabel.text = dateFormatter.string(from: timeStampDate as Date)
            }
            
           
            
        }
    }
    var user: User?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        //The +- 2 is to create more speace between the labels
        textLabel?.frame = CGRect(x: 64, y: (textLabel!.frame.origin.y - 2), width: (textLabel?.frame.width)!, height: (textLabel?.frame.height)!)
        detailTextLabel?.frame = CGRect(x: 64, y: (detailTextLabel!.frame.origin.y + 2), width: (detailTextLabel?.frame.width)!, height: (detailTextLabel?.frame.height)!)
    }
    
    
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = 24
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let timeLabel: UILabel = {
        let label = UILabel()
        label.text = "HH:MM:SS"
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = UIColor.darkGray
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        
        addSubview(profileImageView)
        addSubview(timeLabel)
        
        
        profileImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        profileImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: 48).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant: 48).isActive = true
        
        timeLabel.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        timeLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 18).isActive = true
        //timeLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor)
        timeLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        timeLabel.heightAnchor.constraint(equalTo: (textLabel?.heightAnchor)!).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
